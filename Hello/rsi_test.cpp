//
// Created by zhuruyi on 2021/3/12.
//

#include <MessageFrame.h>
#include "rsi_test.h"

//#include <errno.h>

//typedef enum OutRule {
//    DER,
//    OER,
//    PER,
//    XER
//} e_OutRule;
//
//static int save_to_file(const void *data, size_t size, void *key) {
//    FILE *fp = (FILE *) key;
//    return (fwrite(data, 1, size, fp) == size) ? 0 : -1;
//}
//
//
//int struct_to_file(MessageFrame_t *pFrame, char *outP, e_OutRule rule) {
//    FILE *fpBin;
//    fpBin = fopen(outP, "w+");
//    asn_enc_rval_t er;
//
//    switch (rule) {
//        case DER:
//            er = der_encode(&asn_DEF_MessageFrame, pFrame, save_to_file, fpBin);
//            break;
//        case OER:
//            er = oer_encode(&asn_DEF_MessageFrame, pFrame, save_to_file, fpBin);
//            break;
//        case PER:
//            er = uper_encode(&asn_DEF_MessageFrame, nullptr, pFrame, save_to_file, fpBin);
//            break;
//        case XER:
//            er = xer_encode(&asn_DEF_MessageFrame, pFrame, XER_F_BASIC, save_to_file, fpBin);
//            break;
//        default:
//            er.encoded = -1;
//            break;
//    }
//    if (er.encoded == -1) {
//        fprintf(stderr, "Cannot encode %s: %s\n", er.failed_type->name, strerror(errno));
//        return -1;
//    } else {
///* Return the number of bytes */
//        return er.encoded;
//    }
//}

MessageFrame_t message_frame;

MessageFrame *test_rsi() {
    RoadSideInformation_t roadSideInformation;

    roadSideInformation.msgCnt = 10;
    MinuteOfTheYear_t minuteOfTheYear = 5000;
    roadSideInformation.timeStamp = &minuteOfTheYear;


    asn_struct_ctx_t ctx;

    OCTET_STRING_t id;
    id._asn_ctx = ctx;
    //construct buff
    id.size = 8;
    //buffer size must be  8
    char rsi_id[] = "12345678";
    id.buf = (uint8_t *) rsi_id;
    roadSideInformation.id = id;

    //0-255
    roadSideInformation.rsiId = 200;

    //0-65535 定义路侧警示信息 RSA 的类型  37 danger
    roadSideInformation.alertType = 37;

    roadSideInformation.description = nullptr;
    roadSideInformation.priority = nullptr;

    //34.744998,113.654938
    Position3D_t pos;
    pos._asn_ctx = ctx;
    //ongitude jingdu
    pos.Long = (Longitude_t) 113.654938;
    pos.lat = (Longitude_t) 34.744998;
    // 海拔 分辨率0.1m   -40
    auto elevation = (Elevation_t) 200.1;
    pos.elevation = &elevation;
    roadSideInformation.refPos = pos;

    //消息作用范围的车辆行进轨迹区段。 2..32
    PathPointList_t pathPointList;
    PositionOffsetLLV_t *positionOffsetLlvArr[2];
    //定义三维对位置。（相对经纬度和相对高程）
    PositionOffsetLLV_t positionOffsetLlv1;
    //PositionOffsetLL_t 经纬度偏差，用来描述一个坐标点的相对位置。提供了7种尺度的描述方式。
    //ll1 对应Position-LL-24B， 24 比特相对经纬度位置,表示当前位置点关于参考位置点的经纬度偏差。由两个 12 比特的经度、纬度偏差值组成
    positionOffsetLlv1.offsetLL.present = PositionOffsetLL_PR_position_LL1;
    positionOffsetLlv1.offsetLL.choice.position_LL1.lat = (OffsetLL_B12_t) 34.744998;
    positionOffsetLlv1.offsetLL.choice.position_LL1.lon = (OffsetLL_B12_t) 113.654938;
    positionOffsetLlv1.offsetLL.choice.position_LL1._asn_ctx = ctx;
    positionOffsetLlv1.offsetV = nullptr;
    positionOffsetLlvArr[0] = &positionOffsetLlv1;

    PositionOffsetLLV_t positionOffsetLlv2;
    positionOffsetLlv2.offsetLL.present = PositionOffsetLL_PR_position_LL1;
    positionOffsetLlv2.offsetLL.choice.position_LL1.lat = (OffsetLL_B12_t) 34.744600;
    positionOffsetLlv2.offsetLL.choice.position_LL1.lon = (OffsetLL_B12_t) 113.654458;
    positionOffsetLlv2.offsetLL.choice.position_LL1._asn_ctx = ctx;
    positionOffsetLlv2.offsetV = nullptr;
    positionOffsetLlvArr[1] = &positionOffsetLlv2;

    pathPointList.list.array = (PositionOffsetLLV_t **) &positionOffsetLlvArr;
    pathPointList.list.size = 2;
    pathPointList.list.count = 2;
    pathPointList.list.free = FREE(PositionOffsetLLV);
    roadSideInformation.alertPath = pathPointList;

    //如一个交通事件基于圆心参考位置点的作用范围。分辨率是10cm
    roadSideInformation.alertRadius = 2000;

    roadSideInformation._asn_ctx = ctx;

    //construct MessageFrame
    message_frame.present = MessageFrame_PR_rsiFrame;
    message_frame.choice.rsiFrame = roadSideInformation;

    xer_fprint(stdout, &asn_DEF_MessageFrame, &message_frame);

//    char outFile[] = "/home/zhuruyi/v2x/selfTest/testFile/rsi.der";
//    struct_to_file(&message_frame, outFile, DER);
    return &message_frame;
}
