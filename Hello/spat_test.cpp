//
// Created by zhuruyi on 2021/3/12.
//

#include "spat_test.h"


MessageFrame *test_spat() {
    SPAT_t spat;
    asn_struct_ctx_t ctx;

    //定义名称字符串类型。
    DescriptiveName_t descriptiveName;
    descriptiveName.size = 8;
    char spat_name[] = "spat0012";
    descriptiveName.buf = (uint8_t *) spat_name;
    spat.name = &descriptiveName;
//    cout << "sapt.name==" << spat.name->buf << endl;

    //PhaseState 定义信号灯的一个相位状态。包含该状态对应的灯色,以及实时计时信息。
    PhaseState_t *phaseStateArr[1];
    PhaseState_t phaseState;
    phaseState.light = LightState_stop_And_Remain;//red light state
    phaseState.timing = nullptr;
    phaseStateArr[0] = &phaseState;

    //定义一个信号灯的一个相位中的相位状态列表。列表中每一个相位状态物理上对应一种相位灯色,其属性包括了该状态的实时计时信息。
    PhaseStateList_t phaseStateList;
    phaseStateList.list.array = (PhaseState **) phaseStateArr;
    phaseStateList.list.count = 1;
    phaseStateList.list.size = 1;
    phaseStateList._asn_ctx = ctx;

    //定义信号灯相位。一个相位包括一个相位ID以及一个相位状态列表。
    Phase_t phase;
    //0-255， 0 is not available.
    phase.id = 20;
    phase.phaseStates = phaseStateList;
    Phase_t *phaseArr[1];
    phaseArr[0] = &phase;

    //定义一组信号灯包含的所有相位的列表。
    PhaseList_t phaseList;
    phaseList.list.array = (Phase **) phaseArr;
    phaseList.list.size = 1;
    phaseList.list.count = 1;
    phaseList._asn_ctx = ctx;

    //定义一个路口信号灯的属性和当前状态。包括路口 ID、信号灯工作状态、时间戳以及信号灯的相位列表。
    IntersectionState_t intersectionState;
    //定义参考 ID。参考 ID 由一个全局唯一的地区 ID 和一个地区内部唯一的节点 ID 组成。
    RoadRegulatorID_t roadRegulatorId = 0;//定义地图中各个划分区域的 ID 号。数值 0 仅用于测试。
    intersectionState.intersectionId.region = &roadRegulatorId;
    //定义节点 ID。路网最基本的构成即节点和连接节点的路段。节点可以是路口,也可以是一条
    //路的端点。一个节点的 ID 在同一个区域内是唯一的。数值 0 ~ 255 预留为测试使用。
    intersectionState.intersectionId.id = 10000;
    //IntersectionStatusObject_t 路口信号机的工作状态指示。用于 SPAT 消息中。
    short buff = 0x1;
//    short int buff = 0x0006;
    intersectionState.status.buf = (uint8_t *) &buff;
    intersectionState.status.size = 2;
    intersectionState.status.bits_unused = 3;
    intersectionState.status._asn_ctx = ctx;
    //MinuteOfTheYear_t
    intersectionState.moy = nullptr;
    //DSecond_t
    intersectionState.timeStamp = nullptr;
    intersectionState.phases = phaseList;

    IntersectionStateList_t intersectionStateList;
    IntersectionState_t *intersectionStateArr[1];
    intersectionStateArr[0] = &intersectionState;
    //定义一个路口信号灯集合。
    intersectionStateList.list.array = (IntersectionState **) &intersectionStateArr;
    intersectionStateList.list.size = 1;
    intersectionStateList.list.count = 1;
    spat.intersections = intersectionStateList;

    spat._asn_ctx = ctx;
    spat.msgCnt = 10;
    MinuteOfTheYear_t minuteOfTheYear = 5000;
    spat.timeStamp = &minuteOfTheYear;

    //construct MessageFrame
    MessageFrame_t message_frame;
    message_frame.present = MessageFrame_PR_spatFrame;
    message_frame.choice.spatFrame = spat;
    message_frame._asn_ctx = ctx;

    xer_fprint(stdout, &asn_DEF_MessageFrame, &message_frame);
    return &message_frame;
}