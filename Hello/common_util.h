//
// Created by zhuruyi on 2021/3/12.
//

#ifndef HELLO_COMMON_UTIL_H
#define HELLO_COMMON_UTIL_H
#include "MessageFrame.h"

static int save_to_file(const void *data, size_t size, void *key);
void *XML_to_RSI(char *xml_file_name, char *out_file_name, asn_app_consume_bytes_f *cb);
void *PER_to_XER(char *per_file_name, char *xml_file_name, asn_app_consume_bytes_f *cb);


#endif //HELLO_COMMON_UTIL_H
