//
// Created by zhuruyi on 2021/3/12.
//

#ifndef HELLO_COMMON_FREE_H
#define HELLO_COMMON_FREE_H

#include "MessageFrame.h"
#define FREE(type) FREE_##type

void FREE_PositionOffsetLLV(PositionOffsetLLV_t *&pollv);
void FREE_Phase(Phase *&phase);

#endif //HELLO_COMMON_FREE_H
