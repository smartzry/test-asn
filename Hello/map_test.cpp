//
// Created by zhuruyi on 2021/3/12.
//

#include "map_test.h"

MessageFrame *test_map() {
    MapData_t mapData;
    asn_struct_ctx_t ctx;
    mapData._asn_ctx = ctx;
    mapData.msgCnt = 10;
    mapData.timeStamp = nullptr;

    //定义地图节点列表。
    NodeList_t nodeList;
    nodeList._asn_ctx = ctx;
    nodeList.list.array = (Node **) calloc(1, sizeof(Node_t *));
    nodeList.list.array[0] = (Node *) malloc(sizeof(Node_t));
    nodeList.list.size = 1;
    nodeList.list.count = 1;
    Node_t *p_node = nodeList.list.array[0];
    //定义参考 ID。参考 ID 由一个全局唯一的地区 ID 和一个地区内部唯一的节点 ID 组成。
    RoadRegulatorID_t roadRegulatorId = 0;//定义地图中各个划分区域的 ID 号。数值 0 仅用于测试。
    p_node->id.region = &roadRegulatorId;
    //定义节点 ID。路网最基本的构成即节点和连接节点的路段。节点可以是路口,也可以是一条
    //路的端点。一个节点的 ID 在同一个区域内是唯一的。数值 0 ~ 255 预留为测试使用。
    p_node->id.id = 10000;
    p_node->id._asn_ctx = ctx;
    p_node->name = nullptr;
    p_node->inLinks = nullptr;

    //34.744998,113.654938
    Position3D_t pos;
    pos._asn_ctx = ctx;
    //ongitude jingdu
    pos.Long = (Longitude_t) 113.654938;
    pos.lat = (Longitude_t) 34.744998;
    // 海拔 分辨率0.1m   -40
    auto elevation = (Elevation_t) 200.1;
    pos.elevation = &elevation;
    p_node->refPos = pos;
    p_node->_asn_ctx = ctx;

    mapData.nodes = nodeList;

    //construct MessageFrame
    MessageFrame_t message_frame;
    message_frame.present = MessageFrame_PR_mapFrame;
    message_frame.choice.mapFrame = mapData;
    message_frame._asn_ctx = ctx;

    xer_fprint(stdout, &asn_DEF_MessageFrame, &message_frame);
    free(p_node);
    free(nodeList.list.array);
    return &message_frame;
}