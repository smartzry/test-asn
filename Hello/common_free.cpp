//
// Created by zhuruyi on 2021/3/12.
//

#include "common_free.h"
#include "MessageFrame.h"

void FREE_PositionOffsetLLV(PositionOffsetLLV_t *&pollv) {
    free(pollv);
}

void FREE_Phase(Phase *&phase) {
    free(phase);
}